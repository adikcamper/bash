# bash
## Podstawy Bash'a

### lista plików ze szczegółami
ls -lah  

### przejdź do folderu 'jakisFlder'
cd jakisFlder  

### skasuj rekursywnie(wchodządz w podkatalogi) i pytaj czy napewno.
rm -ri  

### Stwórz 'jakisInnyFolder'
mkdir jakisInnyFolder  

### przeszukaj zaczynając od miejsca "." czyli tutaj.
find .  

### stwór "plik.txt"
touch plik.txt  

### Pokaż zawartość "plik.txt"
cat plik.txt  

### Przenieś plik/folder 

mv jakisPlik.txt jakisFolder/  

### gdzie jestem?
pwd

### Poszukaj zawartości rekursywnie po wszystkich plikach

grep -rn jakisString

